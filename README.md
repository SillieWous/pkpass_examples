# PkPass examples
This repo is a collection of pkpass examples from the wild. The collected passes should be modified to remove any personal information. These passes are meant as a reference for the [pkpass](https://gitlab.com/SillieWous/pkpass) repository.

## Removing personal information
Perform the following steps to remove any personal information from a pass.
1. Extract the `.pkpass` file.
2. Replace any personal information in the `pass.json` file with fake data.
  * If there is order information and serial numbers, replace these with fake data as well.
3. Check that other files do not contain personal information. If they do replace it with fake data.
4. Adjust the sha1sum in `manifest.json` to reflect new files.
5. Re-zip all the files and change the extension to .pkpass

## Directory structure
Depersonalised passes shall be placed in the folder of the pass type. If the pass contains an event ticket it shall be placed in `event_ticket` directory.

| Pass type     | Directory     |
|---------------|---------------|
| Boarding pass | boarding_pass |
| Coupon        | coupon        |
| Event ticket  | event_ticket  |
| Generic       | generic       |
| NFC           | nfc           |
| Store card    | store_card    |
